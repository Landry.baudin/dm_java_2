import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;


public class BibDM {

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
      Integer minim= null;
      if(liste.size()!=0){
      for(Integer nb:liste){
        if (minim==null || nb<minim){
          minim=nb;
        }
      }
    }
      return minim;
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        for(T elem:liste){
          if(valeur.compareTo(elem)>0 || valeur.equals(elem)){
            return false;
          }
        }
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
      List<T> liste3=new ArrayList<T>();
      List<T> li1=new ArrayList<T>();
      List<T> li2=new ArrayList<T>();
      int j=0;
      boolean t=false;
      if(liste1.size()>=liste2.size()){
        li1=liste1;
        li2=liste2;
      }
      else{
        li1=liste2;
        li2=liste1;
      }

      for(T elem:li1){
        for(T el:li2){
          if(el.compareTo(elem)==0){
            for(T e:liste3){
              if(el.compareTo(e)==0){
                t=true;
              }
            }
            if(!t){
              liste3.add(el);
            }
            t=false;
          }
        }
        }
        return liste3;
      }




    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> listeMot=new ArrayList<String> ();
        String mot="";
        int cpt=0;
        char [] carct = texte.toCharArray();

        for(char c:carct){
          if(cpt==texte.length()-1){
            if(c!=' '){
            mot+=Character.toString(c);
          }
            listeMot.add(mot);
          }

          else if(c==' '){
            if(mot.length()>0){
            listeMot.add(mot);
            mot="";
          }
          }

          else{
            mot+=Character.toString(c);
          }
          cpt+=1;
        }
        return listeMot;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
      List<String> listeMot= decoupe(texte);
      HashMap<String,Integer> dico= new HashMap<String,Integer> ();
      int max=0;
      String leMot=null;

      for(String elem:listeMot){
        if(!dico.containsKey(elem)){
          dico.put(elem,1);
        }
        else{
          dico.put(elem,dico.get(elem)+1);
        }
      }

      for(String m:dico.keySet()){
        if(leMot==null){
          leMot=m;
          max=dico.get(m);
        }
        if(dico.get(m)>max){
          leMot=m;
          max=dico.get(m);
        }
        if(dico.get(m)==max && m.compareTo(leMot)<0){
          leMot=m;
        }
      }

        return leMot;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        char [] caractere=chaine.toCharArray();
        int parG=0;
        int parD=0;
        int cpt=0;
        for(char c:caractere){
          if(c=='('){
            if(cpt==chaine.length()-1){
              return false;
            }
            else{
              parG+=1;
            }
          }
          if(c==')'){
            if(parD==0 && parG==0 || parD==parG){
              return false;
            }
            else{
              parD+=1;
            }
          }
          cpt+=1;
        }
        return parG==parD;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
      char [] caractere=chaine.toCharArray();
      int crochG=0;
      int crochD=0;
      int cpt=0;
      char prec=' ';
      for(char c:caractere){
        if(c=='['){
          if(cpt==chaine.length()-1){
            return false;
          }
          else{
            crochG+=1;
          }
        }
        if(c==']'){
          if(crochD==0 && crochG==0 || crochD==crochG){
            return false;
          }
          else{
            crochD+=1;
          }
        }

        if(prec=='(' && c==']' || prec=='[' && c==')'){
          return false;
        }

        cpt+=1;
        prec=c;
      }
      return bienParenthesee(chaine) && crochG==crochD;
    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        boolean trouve=false;
        int idep=0;
        int ifin=liste.size()-1;
        while(!trouve && ifin-idep>1){
          int milieu=(ifin+idep)/2;
          trouve= liste.get(milieu)==valeur;
          if(valeur<liste.get(milieu)){
            ifin=milieu;
          }
          if(valeur>liste.get(milieu)){
            idep=milieu;
          }
          if(liste.get(idep)==valeur||liste.get(ifin)==valeur){
            trouve=true;
          }
        }

        return trouve;
    }

}
